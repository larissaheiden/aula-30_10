package br.ifsc.edu.aula31_10;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferences = getSharedPreferences("meuxml", MODE_PRIVATE);
        editor=sharedPreferences.edit();

        editor.putString("nome","Maria");
        editor.commit();



        Log.d("ValorRecuparadoXML",    sharedPreferences.getString("nome","chave nome não encontrada"));


    }
}